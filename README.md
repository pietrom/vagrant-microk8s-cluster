# Requirements
- [Vagrant](https://www.vagrantup.com)
- A Vagrant [virtualization provider](https://developer.hashicorp.com/vagrant/docs/providers) supported by the `generic/ubuntu2004` VagrantBox: *libvirt*, *virtualbox*, *vmware_desktop*, *hyperv*, or *parallels*

# Usage
```
git clone https://gitlab.com/pietrom/vagrant-microk8s-cluster.git
cd vagrant-microk8s-cluster
vagrant up mercury0
vagrant up mercury1
vagrant up mercury2
vagrant up mercury3
vagrant up mercury4
```

Wait some time and you will have a working five-nodes K8S cluster.

You can get the K8S cluster config for `kubectl` executing `./get-mercury-config.sh` (or logging into the first VM with `vagrant ssh mercury0`, launching `microk8s config` and editing the result changing names and host IP)

Then you can check the cluster state

```
KUBECONFIG="$(pwd)/mercury-config" kubectl get nodes -o wide
```

# Customisation
`Vagrantfile` contains a bunch of constants you can change in order to customize your cluster:

```ruby
CLUSTER_NAME = "mercury"
FIRST_NODE_NAME = "#{CLUSTER_NAME}0"
BASE_IP = "172.31.50"
FIRST_NODE_SUFFIX = 20
NODES_COUNT = 5
```

This is the default configuration: using it 
- you get a five-nodes cluster
- cluster node names are `mercury0` to `mercury4`
- cluster node IPs are `172.31.50.20` to `172.31.50.24`

Feel free to play changing parameters and creating different clusters!