# Cluster configuration
CLUSTER_NAME = "gavia"
FIRST_NODE_NAME = "#{CLUSTER_NAME}0"
BASE_IP = "172.31.60"
FIRST_NODE_SUFFIX = 20
NODES_COUNT = 2
LAST_NODE_SUFFIX = FIRST_NODE_SUFFIX + NODES_COUNT - 1
CPUS_PER_NODE = 2
RAM_PER_NODE_KB = 2048

def define_vm(config, name, baseBox, baseBoxVersion, ips, diskSize = nil, primary = false)
  config.vm.define name, primary: primary, autostart: primary do |node|
    node.vm.box = baseBox
    node.vm.box_version = baseBoxVersion
    ips.each do |ip|
      node.vm.network :private_network, ip: ip, :netmask => "255.255.0.0"
    end
    node.vm.hostname = name
    node.vm.provider "libvirt" do |vb|
      vb.memory = RAM_PER_NODE_KB
      vb.cpus = CPUS_PER_NODE
    end
    node.vm.provider "virtualbox" do |vb|
      vb.memory = RAM_PER_NODE_KB
      vb.cpus = CPUS_PER_NODE
    end
    args = "#{CLUSTER_NAME} #{FIRST_NODE_SUFFIX} #{LAST_NODE_SUFFIX} #{BASE_IP}"
    node.vm.provision "shell", path: "scripts/initial-setup.sh", :args => args
    node.vm.provision "shell", path: "scripts/install-microk8s.sh"
    node.vm.provision "shell", path: "scripts/setup-hosts.sh", :args => args, run: 'always'

    if name != FIRST_NODE_NAME
      node.trigger.after :up do |trigger|
        trigger.run = {
         path: 'scripts/add-node.sh',
         args: [ name, CLUSTER_NAME, BASE_IP ]
       }
      end
    end
  end
end

def define_docker_vm_amd64(config, namePrefix, i, ip)
  define_vm config, "#{namePrefix}#{i}", "generic/ubuntu2004", "4.1.14", ip
end

Vagrant.configure("2") do |config|
  (0..(NODES_COUNT - 1)).each do |i|
    define_docker_vm_amd64 config, CLUSTER_NAME, i, ["#{BASE_IP}.#{FIRST_NODE_SUFFIX + i}"]
  end
end

if ARGV[0] != 'ssh'
  normalized_cluster_name = CLUSTER_NAME
  if CLUSTER_NAME.end_with?("-")
    normalized_cluster_name = CLUSTER_NAME[0..-2]
  end
  get_config_file = "get-#{normalized_cluster_name}-config.sh"
  config_file = "#{normalized_cluster_name}-config"
  get_config_command = "vagrant ssh #{CLUSTER_NAME}0 -c \"microk8s config | sed 's/admin/#{CLUSTER_NAME}-admin/' | sed 's/microk8s/#{CLUSTER_NAME}/' | sed 's/server[:] https.*[:]\\(.*\\)$/server: https:\\/\\/#{BASE_IP}.#{FIRST_NODE_SUFFIX}:\\1/'\" | tee #{config_file}"
  
  File.open(get_config_file, "w+") do |f|
    f.puts(["#!/bin/bash", "", "echo 'Getting k8s config for cluster #{normalized_cluster_name}'", get_config_command])
  end

  puts "K8S config file for cluster #{normalized_cluster_name} written to #{get_config_file}"
end
