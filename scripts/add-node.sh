#!/bin/bash
cluster_name="$2"
cluster_primary_name="${cluster_name}0"
node_name="$1"
base_ip="$3"
echo "    Adding node ${node_name} to cluster ${cluster_name}, Base IP address is ${base_ip}"
join_command=$(vagrant ssh "${cluster_primary_name}" -c "microk8s add-node | grep ${base_ip}")
cleaned_up_join_command=$(tr -dc '[[:print:]]' <<< "$join_command")
# Credits https://stackoverflow.com/questions/22045175/remove-non-printing-chars-from-bash-variable#22045214
echo "    Join command: ${cleaned_up_join_command}"
vagrant ssh "${node_name}" -c "${cleaned_up_join_command}"