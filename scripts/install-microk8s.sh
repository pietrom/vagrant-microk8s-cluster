#!/bin/bash
# Install microk8s through snap
test_command="microk8s"
snap_name="microk8s"
snap_version="latest/stable"

whereis ${test_command} | grep "^${test_command}:$"
if [ $? -eq 0 ]; then
	snap install --classic --channel=${snap_version} ${snap_name}
	microk8s enable dns
	microk8s enable ingress
	microk8s enable metrics-server
	usermod -a -G microk8s vagrant
else
	echo "microk8s already installed"
fi
