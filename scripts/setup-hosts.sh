#!/bin/bash
CLUSTER_NAME=$1
FIRST_NODE_SUFFIX=$2
LAST_NODE_SUFFIX=$3
BASE_IP=$4

echo "    initial-setup.sh"
echo "        CLUSTER_NAME = $CLUSTER_NAME"
echo "        FIRST_NODE_SUFFIX = $FIRST_NODE_SUFFIX"
echo "        LAST_NODE_SUFFIX = $LAST_NODE_SUFFIX"
echo "        BASE_IP = $BASE_IP"

# Configure /etc/hosts
cat /etc/hosts | grep -v $(hostname) > /var/tmp/hosts
mv -f /var/tmp/hosts /etc/hosts

echo " -------- Setting up /etc/hosts --------"
for i in $(seq ${FIRST_NODE_SUFFIX} ${LAST_NODE_SUFFIX}); do
	index=$(expr ${i} - $FIRST_NODE_SUFFIX)
	grep "${CLUSTER_NAME}${index}" /etc/hosts
	if [ $? -ne 0 ] ; then
		echo "${BASE_IP}.${i}	${CLUSTER_NAME}${index}" >> /etc/hosts
	fi
done

cat /etc/hosts